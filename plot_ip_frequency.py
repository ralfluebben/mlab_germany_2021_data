import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rcParams

rcParams['font.family'] = 'serif'
rcParams['font.sans-serif'] = ['Times']
rcParams['lines.markersize'] = 4
rcParams['font.size'] = 12

import matplotlib.pyplot as plt

for city in [ "fra", "ham" ]: 

    fn_data= 'count_ips_busy_hours_'+ city+'.json'
    df_throughput = pd.read_json(fn_data)

    fig, ax = plt.subplots()
    plt.grid(axis = 'y', linestyle = '--')
    m = ["bbr", "cubic"]
    width = 0.24
    my_legend = [ [ "BBR #IPs", 'cubic' ], ["BBR #Mea.", 'cubic'], ["Cubic #IPs", 'bbr'], ["Cubic #Mea.", 'bbr'] ]
    c=-width-width/2
    l_i=0
    for i in m:
        df_hour = df_throughput[( (df_throughput['cc']==i))].groupby("hour").sum()
        ax.bar(df_hour.index+c, df_hour["cip"]/1000, width, label=my_legend[l_i][0])
        c=c+width
        l_i+=1
        ax.bar(df_hour.index+c, df_hour["cm"]/1000, width, label=my_legend[l_i][0])
        c=c+width
        l_i+=1

    plt.xticks(np.arange(0,24))
    plt.xlim((-1,24))
    plt.xlabel("hour of day")
    plt.ylabel("#measurements/IPs [in thousands]")
    if city == "fra":
        plt.legend()
    plt.setp(ax.get_xticklabels(), rotation=90, ha="center", va='top', rotation_mode="default")
    plt.tight_layout()
    fname = "figures/busy_hours_"+ city+"_frequency.pdf"
    plt.savefig(fname)
    plt.close()

df_throughput = pd.read_json('count_measurements_ips.json')
fig, ax = plt.subplots()
plt.grid(axis = 'y', linestyle = '--')
x = []
y = []
for n in range(1,df_throughput['cm'].max()+1):
    r=df_throughput[((df_throughput['cm']==n))]["cm"].sum()
    print(n, r, r/n, r/df_throughput["cm"].sum())
    x.append(n)
    y.append(r/df_throughput["cm"].sum())

ax.bar(x,y,0.95)
plt.xlabel("#measurements per IP in busy hour")
plt.ylabel("ratio on total measurements")
plt.yscale("log")
plt.legend()
fname = "figures/ratio_clients.pdf"
plt.tight_layout()
plt.savefig(fname)


m = df_throughput["asn"].unique()
num_ips=11
width = 0.9*(1.0/num_ips)
c=-num_ips/2.0*width+width/2
l_i=0

fig, ax = plt.subplots(figsize= [(2*6.4), 4.8])
plt.grid(axis = 'y', linestyle = '--')
for n in range(1,num_ips):
    df_asn=df_throughput[((df_throughput['cm']==n))].groupby("asn").sum()["cm"]/df_throughput[((df_throughput['cm']==n))].groupby("asn").sum()["cm"].sum()
    if len(df_asn.index) < len(m):
        print("Reindex")
        df_asn=df_asn.reindex(m, fill_value=0)
    l="#Mea. " + str(n)
    ax.bar(np.arange(0,len(m))+c, df_asn, width, label=l)
    c=c+width
    l_i+=1

plt.xticks(range(0,len(m)),m)
plt.xlabel("ASN")
plt.ylabel("ratio for replicated\nmeasurements for ASN")
plt.yscale("log")
plt.legend(ncol=2)
plt.tight_layout()
fname = "figures/ratio_asn.pdf"
plt.savefig(fname)

