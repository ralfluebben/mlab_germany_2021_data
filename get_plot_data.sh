#!/bin/bash
GETDATA=false
DATAFILE="data.zip"

if ( $GETDATA ); then
  echo "Data is requested from MLab big query, that can take a while"
else
  if [ -f "$DATAFILE" ]; then
    echo "Use local data file"
    unzip $DATAFILE
  else
    echo "Local data file does not exist, try downloading."
  fi
fi

if ( $GETDATA ); then
  bash get_asns.sh
fi
python3 asnumber_to_text.py

if ( $GETDATA ); then
  bash get_busy_hours.sh
fi
python3 plot_ip_frequency.py
python3 plot_busy_hours_2021.py

if ( $GETDATA ); then
  bash get_throughput_aug.sh
fi
python3 plot_cdf_num_measurements.py

if ( $GETDATA ); then
  bash get_busy_days.sh
fi
python3 plot_busy_days.py

if ( $GETDATA ); then
  bash get_throughput_per_month.sh
fi
python3 plot_throughput_rtt_per_machine.py
python3 plot_throughput_per_month.py

if ( $GETDATA ); then
  bash get_provider_cdf.sh
fi
python3 plot_asns_bar.py