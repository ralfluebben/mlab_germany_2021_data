bq query --format=prettyjson --max_rows=1000000 --use_legacy_sql=false \
'
select count(ClientASN) as cm, 
ClientASN as asnumber, 
tcpinfo.Client.Network.asname asname,
from measurement-lab.ndt.tcpinfo
where tcpinfo.partition_date >= date("2021-08-01") and
tcpinfo.partition_date < date("2021-09-01") and
tcpinfo.server.Geo.Country_Code="DE" and
tcpinfo.client.Geo.Country_Code="DE" and
tcpinfo.server.Geo.city="Hamburg" and
tcpinfo.Client.Network.asname !=""
group by asnumber, asname
order by cm desc
' > asnumber_provider_hamburg_aug2021.json


bq query --format=prettyjson --max_rows=1000000 --use_legacy_sql=false \
'
select count(ClientASN) as cm, 
ClientASN as asnumber, 
tcpinfo.Client.Network.asname asname,
from measurement-lab.ndt.tcpinfo
where tcpinfo.partition_date >= date("2021-08-01") and
tcpinfo.partition_date < date("2021-09-01") and
tcpinfo.server.Geo.Country_Code="DE" and
tcpinfo.client.Geo.Country_Code="DE" and
tcpinfo.server.Geo.city="Frankfurt" and
tcpinfo.Client.Network.asname !=""
group by asnumber, asname
order by cm desc
' > asnumber_provider_frankfurt_aug2021.json


