# Data Repository for the paper *Exploring the Measurement Lab Open Dataset for Internet Performance Evaluation: The German Internet Landscape*

## Prerequisite

- Scripts were tested with Ubuntu 20.04 LTS
- A snapshot of the data is located in the file `data.zip`, also scripts to download all data are enclosed.

### To download the data
- Subscribe your Google account to the M-Lab Discuss group, see https://www.measurementlab.net/data/docs/bq/quickstart/
- Install Google Cloud SDK to use BigQuery command line tool `bq` see:
  - https://cloud.google.com/sdk/docs/install
  - https://cloud.google.com/sdk/docs/initializing
  - https://www.measurementlab.net/data/docs/gcs/

## Get data, template, and plot figures

Set `GETDATA` to true in `get_plot_data.sh` to get all data from Big Query and execute.
```
bash get_plot_data.sh
```
to download and plot all figures.

## Data License

Data license is [No Rights Reserved Creative Commons Zero Waiver](http://creativecommons.org/about/cc0), see https://www.measurementlab.net/data/
