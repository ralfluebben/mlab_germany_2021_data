bq query --format=prettyjson --max_rows=1000000 --use_legacy_sql=false \
'SELECT LAST_DAY(date) as mon,
AVG(ndt.a.MeanThroughputMbps) as avg_throughput,
ndt.Client.Network.asnumber as asn,
COUNT(ndt.a.MeanThroughputMbps) as cm,
APPROX_QUANTILES(a.MeanThroughputMbps, 100)[OFFSET(51)] as med_throughput,
APPROX_QUANTILES(a.MeanThroughputMbps, 100)[OFFSET(26)] as q25_throughput,
APPROX_QUANTILES(a.MeanThroughputMbps, 100)[OFFSET(76)] as q75_throughput,
APPROX_QUANTILES(a.MeanThroughputMbps, 100) as cdf_throughput,
AVG(a.MinRTT) as avg_minRtt,
APPROX_QUANTILES(a.MinRTT, 100)[OFFSET(51)] as med_minRtt,
APPROX_QUANTILES(a.MinRTT, 100)[OFFSET(26)] as q25_minRtt,
APPROX_QUANTILES(a.MinRTT, 100)[OFFSET(76)] as q75_minRtt,
APPROX_QUANTILES(a.MinRTT, 100) as cdf_minRtt,
FROM `measurement-lab.ndt.unified_downloads` as ndt
where ndt.date > date("2020-01-01") and
ndt.date < date("2021-09-01") and
ndt.server.Geo.CountryCode="DE" and
ndt.client.Geo.CountryCode="DE" and
ndt.server.Geo.city = "Hamburg" and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") >= 20 and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") < 22 and
ndt.Client.Network.asnumber in (3320, 3209, 6805, 9145, 8881, 15943, 13045, 20676, 20880, 60294) 
group by mon, asn order by mon' > throughput_asns_hamburg.json

bq query --format=prettyjson --max_rows=1000000 --use_legacy_sql=false \
'SELECT LAST_DAY(date) as mon,
AVG(ndt.a.MeanThroughputMbps) as avg_throughput,
ndt.Client.Network.asnumber as asn,
COUNT(ndt.a.MeanThroughputMbps) as cm,
APPROX_QUANTILES(a.MeanThroughputMbps, 100)[OFFSET(51)] as med_throughput,
APPROX_QUANTILES(a.MeanThroughputMbps, 100)[OFFSET(26)] as q25_throughput,
APPROX_QUANTILES(a.MeanThroughputMbps, 100)[OFFSET(76)] as q75_throughput,
APPROX_QUANTILES(a.MeanThroughputMbps, 100) as cdf_throughput,
AVG(a.MinRTT) as avg_minRtt,
APPROX_QUANTILES(a.MinRTT, 100)[OFFSET(51)] as med_minRtt,
APPROX_QUANTILES(a.MinRTT, 100)[OFFSET(26)] as q25_minRtt,
APPROX_QUANTILES(a.MinRTT, 100)[OFFSET(76)] as q75_minRtt,
APPROX_QUANTILES(a.MinRTT, 100) as cdf_minRtt,
FROM `measurement-lab.ndt.unified_downloads` as ndt
where ndt.date > date("2020-01-01") and
ndt.date < date("2021-09-01") and
ndt.server.Geo.CountryCode="DE" and
ndt.client.Geo.CountryCode="DE" and
ndt.server.Geo.city = "Frankfurt" and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") >= 20 and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") < 22 and
ndt.Client.Network.asnumber in (3320, 3209, 6805, 198570, 8881, 8422, 42652, 20676, 60294, 41998) 
group by mon, asn order by mon' > throughput_asns_frankfurt.json