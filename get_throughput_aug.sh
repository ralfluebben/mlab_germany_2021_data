#!/bin/bash
d=2021-07-31 #d is not included
while [ "$d" != 2021-08-31 ]; do  #this date is included
  d=$(date -I -d "$d + 1 day")
  echo $d
  tfile=$(mktemp ./foo.XXXXXXXXX)
  sed "s/STARTDATE/${d}/g" get_throughput_aug.temp.sh > $tfile
  bash $tfile
  rm $tfile
done
