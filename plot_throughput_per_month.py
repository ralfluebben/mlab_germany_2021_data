import pandas as pd
import numpy as np
from datetime import datetime, date
from dateutil import relativedelta

from matplotlib import rcParams

rcParams['font.family'] = 'serif'
rcParams['font.sans-serif'] = ['Times']
rcParams['lines.markersize'] = 4
rcParams['font.size'] = 12

import matplotlib.pyplot as plt

def get_forward_month_list(start_date):
    start_month = date.fromisoformat(start_date)
    return [(start_month - relativedelta.relativedelta(months=i)).strftime('%b %y') for i in range(0,32)]

fn_data = 'throughput_per_month_asns_busy_hours.json'
df_throughput = pd.read_json(fn_data)

months_since_begin = []
begin_of_period = '2019-01-31'
date2 = datetime.strptime(begin_of_period, '%Y-%m-%d')

for i in range(df_throughput.shape[0]):
    date1 = datetime.strptime(df_throughput['mon'].iloc[i], '%Y-%m-%d')
    r = relativedelta.relativedelta(date1, date2)
    months_since_begin.append(r.months + (12*r.years))

df_throughput['months_since_begin'] = months_since_begin

month_labels = get_forward_month_list("2021-08-01")
month_labels.reverse()

fig = plt.figure()
ax = fig.add_subplot(111)
index_bbr = 19
index_cubic = 10

df_bbr = df_throughput[((df_throughput['cc'] == "bbr") & ( (df_throughput['site'] == "fra01") | (df_throughput['site'] == "fra02") | (df_throughput['site'] == "fra03") ))]
df_bbr_agg = df_bbr.groupby(['months_since_begin']).mean()

df_cub = df_throughput[((df_throughput['cc'] == "cubic")  & ( (df_throughput['site'] == "fra01") | (df_throughput['site'] == "fra02") | (df_throughput['site'] == "fra03")  ) )]
df_cub_agg = df_cub.groupby(['months_since_begin']).mean()

df_ren = df_throughput[((df_throughput['cc'] == "reno")  & ( (df_throughput['site'] == "fra01") | (df_throughput['site'] == "fra02") | (df_throughput['site'] == "fra03")   ))]
df_ren_agg = df_ren.groupby(['months_since_begin']).mean()

d1 = df_bbr_agg.index.to_numpy()
d2 = df_cub_agg.index.to_numpy()
d3 = df_ren_agg.index.to_numpy()

idx_max = np.max([np.max(d1), np.max(d2), np.max(d3)])
x = new_index = np.arange(0, idx_max+1)

df_bbr_agg = df_bbr_agg.reindex(new_index, fill_value=0)
df_cub_agg = df_cub_agg.reindex(new_index, fill_value=0)
df_ren_agg = df_ren_agg.reindex(new_index, fill_value=0)

data_tag = 'med_throughput'
y1 = df_bbr_agg[data_tag].to_numpy()
y2 = df_cub_agg[data_tag].to_numpy()
y3 = df_ren_agg[data_tag].to_numpy()

data_tag = 'q25_throughput'
y1_q25 = df_bbr_agg[data_tag].to_numpy()
y2_q25 = df_cub_agg[data_tag].to_numpy()
y3_q25 = df_ren_agg[data_tag].to_numpy()

data_tag = 'q75_throughput'
y1_q75 = df_bbr_agg[data_tag].to_numpy()
y2_q75 = df_cub_agg[data_tag].to_numpy()
y3_q75 = df_ren_agg[data_tag].to_numpy()

fig, ax = plt.subplots()
width = 0.5

data_tag = 'q25_throughput'
y1_q25 = df_bbr_agg[data_tag].to_numpy()
y2_q25 = df_cub_agg[data_tag].to_numpy()
y3_q25 = df_ren_agg[data_tag].to_numpy()

data_tag = 'q75_throughput'
y1_q75 = df_bbr_agg[data_tag].to_numpy()
y2_q75 = df_cub_agg[data_tag].to_numpy()
y3_q75 = df_ren_agg[data_tag].to_numpy()

yerr_1 = [y1_q25[index_bbr:], y1_q75[index_bbr:]]
yerr_2 = [y2_q25[index_cubic:index_bbr], y2_q75[index_cubic:index_bbr]]
yerr_3 = [y3_q25[:index_cubic], y3_q75[:index_cubic]]

ax.bar(x[index_bbr:], y1[index_bbr:], width, yerr=yerr_1, label='BBR')
ax.bar(x[index_cubic:index_bbr], y2[index_cubic:index_bbr],
        width, yerr=yerr_2, label='Cubic')
ax.bar(x[:index_cubic], y3[:index_cubic], width, yerr=yerr_3, label='Reno')

#plt.legend(loc=2)
plt.xlabel("month")
plt.ylabel("throughput [MBit/s]")
plt.xlim(-1, 33)
plt.ylim(0, 95)
xticks = np.arange(0,32)
plt.xticks(xticks,month_labels)
plt.setp(ax.get_xticklabels(), rotation=90, ha="right", va='center', rotation_mode="anchor")
plt.grid(axis = 'y', linestyle = '--')
plt.tight_layout()
fname = "figures/throughput_fra.pdf"
plt.savefig(fname)
plt.close()

fig, ax = plt.subplots()

data_tag = 'med_minRtt'
y1 = df_bbr_agg[data_tag].to_numpy()
y2 = df_cub_agg[data_tag].to_numpy()
y3 = df_ren_agg[data_tag].to_numpy()

data_tag = 'q25_minRtt'
y1_q25 = df_bbr_agg[data_tag].to_numpy()
y2_q25 = df_cub_agg[data_tag].to_numpy()
y3_q25 = df_ren_agg[data_tag].to_numpy()

data_tag = 'q75_minRtt'
y1_q75 = df_bbr_agg[data_tag].to_numpy()
y2_q75 = df_cub_agg[data_tag].to_numpy()
y3_q75 = df_ren_agg[data_tag].to_numpy()

yerr_1 = [y1_q25[index_bbr:], y1_q75[index_bbr:]]
yerr_2 = [y2_q25[index_cubic:index_bbr], y2_q75[index_cubic:index_bbr]]
yerr_3 = [y3_q25[:index_cubic], y3_q75[:index_cubic]]

ax.bar(x[index_bbr:], y1[index_bbr:], width, yerr=yerr_1, label='BBR')
ax.bar(x[index_cubic:index_bbr], y2[index_cubic:index_bbr],
        width, yerr=yerr_2, label='Cubic')
ax.bar(x[:index_cubic], y3[:index_cubic], width, yerr=yerr_3, label='Reno')
#plt.legend(loc=2)
plt.xlabel("month")
plt.ylabel("RTT [ms]")
fname = "figures/rtt_fra.pdf"
plt.xlim(-1, 33)
plt.ylim(0, 105)
xticks = np.arange(0,32)
plt.xticks(xticks,month_labels)
plt.setp(ax.get_xticklabels(), rotation=90, ha="right", va='center', rotation_mode="anchor")
plt.grid(axis = 'y', linestyle = '--')
plt.tight_layout()
plt.savefig(fname)
plt.close()

df_bbr_agg = df_bbr.groupby(['months_since_begin']).sum()
df_cub_agg = df_cub.groupby(['months_since_begin']).sum()
df_ren_agg = df_ren.groupby(['months_since_begin']).sum()

df_bbr_agg = df_bbr_agg.reindex(new_index, fill_value=0)
df_cub_agg = df_cub_agg.reindex(new_index, fill_value=0)
df_ren_agg = df_ren_agg.reindex(new_index, fill_value=0)

fig, ax = plt.subplots()

data_tag = 'cm'
y1 = df_bbr_agg[data_tag].to_numpy()/1e3
y2 = df_cub_agg[data_tag].to_numpy()/1e3
y3 = df_ren_agg[data_tag].to_numpy()/1e3

width = 0.35

ax.bar(x, y1, width, label='BBR')
ax.bar(x, y2, width, bottom=y1, label='Cubic')
ax.bar(x, y3, width, bottom=y1+y2, label='Reno')

plt.legend(loc=2)
plt.xlabel("month")
plt.ylabel("#measurements [in thousand]")
fname = "figures/count_fra.pdf"
plt.xlim(-1, 33)
plt.ylim(0, 200)
xticks = np.arange(0,32)
plt.xticks(xticks,month_labels)
plt.setp(ax.get_xticklabels(), rotation=90, ha="right", va='center', rotation_mode="anchor")
plt.grid(axis = 'y', linestyle = '--')
plt.tight_layout()
plt.savefig(fname)
plt.close()


### ham

fig = plt.figure()
ax = fig.add_subplot(111)
index_bbr = 19
index_cubic = 10

df_bbr = df_throughput[((df_throughput['cc'] == "bbr") &  (df_throughput['site'] == "ham02"))]
df_bbr_agg = df_bbr.groupby(['months_since_begin']).mean()

df_cub = df_throughput[((df_throughput['cc'] == "cubic")  &  (df_throughput['site'] == "ham02"))]
df_cub_agg = df_cub.groupby(['months_since_begin']).mean()

df_ren = df_throughput[((df_throughput['cc'] == "reno")  &  (df_throughput['site'] == "ham02"))]
df_ren_agg = df_ren.groupby(['months_since_begin']).mean()

d1 = df_bbr_agg.index.to_numpy()
d2 = df_cub_agg.index.to_numpy()
d3 = df_ren_agg.index.to_numpy()

idx_max = np.max([np.max(d1), np.max(d2), np.max(d3)])
x = new_index = np.arange(0, idx_max+1)

df_bbr_agg = df_bbr_agg.reindex(new_index, fill_value=0)
df_cub_agg = df_cub_agg.reindex(new_index, fill_value=0)
df_ren_agg = df_ren_agg.reindex(new_index, fill_value=0)

data_tag = 'med_throughput'
y1 = df_bbr_agg[data_tag].to_numpy()
y2 = df_cub_agg[data_tag].to_numpy()
y3 = df_ren_agg[data_tag].to_numpy()

data_tag = 'q25_throughput'
y1_q25 = df_bbr_agg[data_tag].to_numpy()
y2_q25 = df_cub_agg[data_tag].to_numpy()
y3_q25 = df_ren_agg[data_tag].to_numpy()

data_tag = 'q75_throughput'
y1_q75 = df_bbr_agg[data_tag].to_numpy()
y2_q75 = df_cub_agg[data_tag].to_numpy()
y3_q75 = df_ren_agg[data_tag].to_numpy()

fig, ax = plt.subplots()
width = 0.5

data_tag = 'q25_throughput'
y1_q25 = df_bbr_agg[data_tag].to_numpy()
y2_q25 = df_cub_agg[data_tag].to_numpy()
y3_q25 = df_ren_agg[data_tag].to_numpy()

data_tag = 'q75_throughput'
y1_q75 = df_bbr_agg[data_tag].to_numpy()
y2_q75 = df_cub_agg[data_tag].to_numpy()
y3_q75 = df_ren_agg[data_tag].to_numpy()

yerr_1 = [y1_q25[index_bbr:], y1_q75[index_bbr:]]
yerr_2 = [y2_q25[index_cubic:index_bbr], y2_q75[index_cubic:index_bbr]]
yerr_3 = [y3_q25[:index_cubic], y3_q75[:index_cubic]]

ax.bar(x[index_bbr:], y1[index_bbr:], width, yerr=yerr_1, label='BBR')
ax.bar(x[index_cubic:index_bbr], y2[index_cubic:index_bbr],
        width, yerr=yerr_2, label='Cubic')
ax.bar(x[:index_cubic], y3[:index_cubic], width, yerr=yerr_3, label='Reno')

#plt.legend(loc=2)
plt.xlabel("month")
plt.ylabel("throughput [MBit/s]")
plt.xlim(-1, 33)
plt.ylim(0, 95)
xticks = np.arange(0,32)
plt.xticks(xticks,month_labels)
plt.setp(ax.get_xticklabels(), rotation=90, ha="right", va='center', rotation_mode="anchor")
plt.grid(axis = 'y', linestyle = '--')
plt.tight_layout()
fname = "figures/throughput_ham.pdf"
plt.savefig(fname)
plt.close()

fig, ax = plt.subplots()

data_tag = 'med_minRtt'
y1 = df_bbr_agg[data_tag].to_numpy()
y2 = df_cub_agg[data_tag].to_numpy()
y3 = df_ren_agg[data_tag].to_numpy()

data_tag = 'q25_minRtt'
y1_q25 = df_bbr_agg[data_tag].to_numpy()
y2_q25 = df_cub_agg[data_tag].to_numpy()
y3_q25 = df_ren_agg[data_tag].to_numpy()

data_tag = 'q75_minRtt'
y1_q75 = df_bbr_agg[data_tag].to_numpy()
y2_q75 = df_cub_agg[data_tag].to_numpy()
y3_q75 = df_ren_agg[data_tag].to_numpy()

yerr_1 = [y1_q25[index_bbr:], y1_q75[index_bbr:]]
yerr_2 = [y2_q25[index_cubic:index_bbr], y2_q75[index_cubic:index_bbr]]
yerr_3 = [y3_q25[:index_cubic], y3_q75[:index_cubic]]

ax.bar(x[index_bbr:], y1[index_bbr:], width, yerr=yerr_1, label='BBR')
ax.bar(x[index_cubic:index_bbr], y2[index_cubic:index_bbr],
        width, yerr=yerr_2, label='Cubic')
ax.bar(x[:index_cubic], y3[:index_cubic], width, yerr=yerr_3, label='Reno')
#plt.legend(loc=2)
plt.xlabel("month")
plt.ylabel("RTT [ms]")
fname = "figures/rtt_ham.pdf"
plt.xlim(-1, 33)
plt.ylim(0, 105)
xticks = np.arange(0,32)
plt.xticks(xticks,month_labels)
plt.setp(ax.get_xticklabels(), rotation=90, ha="right", va='center', rotation_mode="anchor")
plt.grid(axis = 'y', linestyle = '--')
plt.tight_layout()
plt.savefig(fname)
plt.close()

df_bbr_agg = df_bbr.groupby(['months_since_begin']).sum()
df_cub_agg = df_cub.groupby(['months_since_begin']).sum()
df_ren_agg = df_ren.groupby(['months_since_begin']).sum()

df_bbr_agg = df_bbr_agg.reindex(new_index, fill_value=0)
df_cub_agg = df_cub_agg.reindex(new_index, fill_value=0)
df_ren_agg = df_ren_agg.reindex(new_index, fill_value=0)

fig, ax = plt.subplots()

data_tag = 'cm'
y1 = df_bbr_agg[data_tag].to_numpy()/1e3
y2 = df_cub_agg[data_tag].to_numpy()/1e3
y3 = df_ren_agg[data_tag].to_numpy()/1e3

width = 0.35

ax.bar(x, y1, width, label='BBR')
ax.bar(x, y2, width, bottom=y1, label='Cubic')
ax.bar(x, y3, width, bottom=y1+y2, label='Reno')

#plt.legend(loc=2)
plt.xlabel("month")
plt.ylabel("#measurements [in housands]")
fname = "figures/count_ham.pdf"
plt.xlim(-1, 33)
plt.ylim(0, 200)
xticks = np.arange(0,32)
plt.xticks(xticks,month_labels)
plt.setp(ax.get_xticklabels(), rotation=90, ha="right", va='center', rotation_mode="anchor")
plt.grid(axis = 'y', linestyle = '--')
plt.tight_layout()
plt.savefig(fname)
plt.close()