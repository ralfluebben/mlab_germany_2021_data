bq query --format=prettyjson --max_rows=1000000 --use_legacy_sql=false \
'
SELECT ndt.a.MeanThroughputMbps as th,
FROM `measurement-lab.ndt.unified_downloads` as ndt 
where ndt.date = date("STARTDATE") and
ndt.server.Geo.CountryCode="DE" and
ndt.client.Geo.CountryCode="DE" and
ndt.a.CongestionControl="bbr" and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") >= 20 and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") < 22 and
((ndt.Client.Network.asnumber in (198570, 8881, 8422, 42652, 20676, 60294, 41998) and ndt.server.Geo.City="Frankfurt") or 
(ndt.Client.Network.asnumber in (9145, 8881, 15943, 13045, 20676, 20880, 60294) and ndt.server.Geo.City="Hamburg"))
and ndt.Client.IP in ( SELECT ndt.Client.IP as ip
FROM `measurement-lab.ndt.unified_downloads` as ndt 
where ndt.date = date("STARTDATE") and
ndt.server.Geo.CountryCode="DE" and
ndt.client.Geo.CountryCode="DE" and
ndt.a.CongestionControl="bbr" and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") >= 20 and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") < 22 and
((ndt.Client.Network.asnumber in (198570, 8881, 8422, 42652, 20676, 60294, 41998) and ndt.server.Geo.City="Frankfurt") or 
(ndt.Client.Network.asnumber in (9145, 8881, 15943, 13045, 20676, 20880, 60294) and ndt.server.Geo.City="Hamburg"))
group by ip
HAVING COUNT(ndt.Client.IP) > 1 )
' > throughput_STARTDATE_more_measurement_non_cellular.json

bq query --format=prettyjson --max_rows=1000000 --use_legacy_sql=false \
'
SELECT ndt.a.MeanThroughputMbps as th,
FROM `measurement-lab.ndt.unified_downloads` as ndt 
where ndt.date = date("STARTDATE") and
ndt.server.Geo.CountryCode="DE" and
ndt.client.Geo.CountryCode="DE" and
ndt.a.CongestionControl="bbr" and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") >= 20 and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") < 22 and
((ndt.Client.Network.asnumber in (198570, 8881, 8422, 42652, 20676, 60294, 41998) and ndt.server.Geo.City="Frankfurt") or 
(ndt.Client.Network.asnumber in (9145, 8881, 15943, 13045, 20676, 20880, 60294) and ndt.server.Geo.City="Hamburg"))
and ndt.Client.IP in ( SELECT ndt.Client.IP as ip
FROM `measurement-lab.ndt.unified_downloads` as ndt 
where ndt.date = date("STARTDATE") and
ndt.server.Geo.CountryCode="DE" and
ndt.client.Geo.CountryCode="DE" and
ndt.a.CongestionControl="bbr" and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") >= 20 and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") < 22 and
((ndt.Client.Network.asnumber in (198570, 8881, 8422, 42652, 20676, 60294, 41998) and ndt.server.Geo.City="Frankfurt") or 
(ndt.Client.Network.asnumber in (9145, 8881, 15943, 13045, 20676, 20880, 60294) and ndt.server.Geo.City="Hamburg"))
group by ip
HAVING COUNT(ndt.Client.IP) = 1 )
' > throughput_STARTDATE_one_measurement_non_cellular.json

bq query --format=prettyjson --max_rows=1000000 --use_legacy_sql=false \
'
SELECT ndt.a.MeanThroughputMbps as th,
FROM `measurement-lab.ndt.unified_downloads` as ndt 
where ndt.date = date("STARTDATE") and
ndt.server.Geo.CountryCode="DE" and
ndt.client.Geo.CountryCode="DE" and
ndt.a.CongestionControl="bbr" and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") >= 20 and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") < 22 and
((ndt.Client.Network.asnumber in (3320, 3209, 6805, 198570, 8881, 8422, 42652, 20676, 60294, 41998) and ndt.server.Geo.City="Frankfurt") or 
(ndt.Client.Network.asnumber in (3320, 3209, 6805, 9145, 8881, 15943, 13045, 20676, 20880, 60294) and ndt.server.Geo.City="Hamburg"))
and ndt.Client.IP in ( SELECT ndt.Client.IP as ip
FROM `measurement-lab.ndt.unified_downloads` as ndt 
where ndt.date = date("STARTDATE") and
ndt.server.Geo.CountryCode="DE" and
ndt.client.Geo.CountryCode="DE" and
ndt.a.CongestionControl="bbr" and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") >= 20 and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") < 22 and
((ndt.Client.Network.asnumber in (3320, 3209, 6805, 198570, 8881, 8422, 42652, 20676, 60294, 41998) and ndt.server.Geo.City="Frankfurt") or 
(ndt.Client.Network.asnumber in (3320, 3209, 6805, 9145, 8881, 15943, 13045, 20676, 20880, 60294) and ndt.server.Geo.City="Hamburg"))
group by ip
HAVING COUNT(ndt.Client.IP) > 1 )
' > throughput_STARTDATE_more_measurement.json

bq query --format=prettyjson --max_rows=1000000 --use_legacy_sql=false \
'
SELECT ndt.a.MeanThroughputMbps as th,
FROM `measurement-lab.ndt.unified_downloads` as ndt 
where ndt.date = date("STARTDATE") and
ndt.server.Geo.CountryCode="DE" and
ndt.client.Geo.CountryCode="DE" and
ndt.a.CongestionControl="bbr" and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") >= 20 and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") < 22 and
((ndt.Client.Network.asnumber in (3320, 3209, 6805, 198570, 8881, 8422, 42652, 20676, 60294, 41998) and ndt.server.Geo.City="Frankfurt") or 
(ndt.Client.Network.asnumber in (3320, 3209, 6805, 9145, 8881, 15943, 13045, 20676, 20880, 60294) and ndt.server.Geo.City="Hamburg"))
and ndt.Client.IP in ( SELECT ndt.Client.IP as ip
FROM `measurement-lab.ndt.unified_downloads` as ndt 
where ndt.date = date("STARTDATE") and
ndt.server.Geo.CountryCode="DE" and
ndt.client.Geo.CountryCode="DE" and
ndt.a.CongestionControl="bbr" and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") >= 20 and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") < 22 and
((ndt.Client.Network.asnumber in (3320, 3209, 6805, 198570, 8881, 8422, 42652, 20676, 60294, 41998) and ndt.server.Geo.City="Frankfurt") or 
(ndt.Client.Network.asnumber in (3320, 3209, 6805, 9145, 8881, 15943, 13045, 20676, 20880, 60294) and ndt.server.Geo.City="Hamburg"))
group by ip
HAVING COUNT(ndt.Client.IP) = 1 )
' > throughput_STARTDATE_one_measurement.json

