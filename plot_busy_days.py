import pandas as pd
import numpy as np
from datetime import datetime
from dateutil import relativedelta
from matplotlib import rcParams
rcParams['font.family'] = 'serif'
rcParams['font.sans-serif'] = ['Times']
rcParams['lines.markersize'] = 4
rcParams['font.size'] = 12

import matplotlib.pyplot as plt

df_throughput = pd.read_json('throughput_busy_days_busy_hours.json')

months_since_begin = []
begin_of_period = '2019-01-31'
date2 = datetime.strptime(begin_of_period, '%Y-%m-%d')

for i in range(df_throughput.shape[0]):
    date1 = datetime.strptime(df_throughput['mon'].iloc[i], '%Y-%m-%d')
    r = relativedelta.relativedelta(date1, date2)
    months_since_begin.append(r.months + (12*r.years))

df_throughput['months_since_begin'] = months_since_begin

fig, ax = plt.subplots()
m = range(0,7)
width = 0.9
my_legend = [ "Sun" , "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
c=-len(m)/2.0*width+width/2
l_i=0
for i in m:
  df_day = df_throughput[((df_throughput['months_since_begin']==31) & (df_throughput['weekday']==i+1))]
  yerr = [df_day["q25_throughput"], df_day["q75_throughput"]]
  ax.bar(l_i, df_day["med_throughput"], width)
  c=c+width
  l_i+=1

plt.xticks(np.arange(0,7), labels=my_legend)
plt.xlabel("weekday")
plt.ylabel("throughput [Mbps]")
plt.xlim((-1,7))
plt.ylim((0,30))
fname = "figures/busy_days_throughput.pdf"
plt.savefig(fname)
plt.close()


fig, ax = plt.subplots()
m = range(0,7)
width = 0.9
my_legend = [ "Sun" , "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
c=-len(m)/2.0*width+width/2
l_i=0
for i in m:
  df_day = df_throughput[((df_throughput['months_since_begin']==31) & (df_throughput['weekday']==i+1))]
  yerr = [df_day["q25_minRtt"], df_day["q75_minRtt"]]
  ax.bar(l_i, df_day["med_minRtt"], width)
  c=c+width
  l_i+=1

plt.xticks(np.arange(0,7), labels=my_legend)
plt.xlabel("weekday")
plt.ylabel("RTT [ms]")
plt.xlim((-1,7))
plt.ylim((0,25))
fname = "figures/busy_days_minrtt.pdf"
plt.savefig(fname)
plt.close()