import json

with open('asnumber_provider_hamburg_aug2021.json') as json_file:
    data = json.load(json_file)
    count_measurements = 0
    for p in data:
      count_measurements += int(p['cm'])
    limit=11
    total_freq=0.0
    for p in data:
        total_freq += float(p['cm'])/count_measurements
        freq=round(float(p['cm'])/count_measurements,2)
        
        print(p['asname'], p['asnumber'],  int(p['cm']), "{:.2f}".format(freq)  , sep=" & ", end="\\\\\n")
        limit-=1
        if limit == 0:
          break

with open('asnumber_provider_frankfurt_aug2021.json') as json_file:
    data = json.load(json_file)
    count_measurements = 0
    for p in data:
      count_measurements += int(p['cm'])
    limit=11
    total_freq=0.0
    for p in data:
        total_freq += float(p['cm'])/count_measurements
        freq=round(float(p['cm'])/count_measurements,2)
        
        print(p['asname'], p['asnumber'],  int(p['cm']), "{:.2f}".format(freq)  , sep=" & ", end="\\\\\n")
        limit-=1
        if limit == 0:
          break