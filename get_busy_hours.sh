bq query --format=prettyjson --max_rows=1000000 --use_legacy_sql=false \
'
SELECT EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") as hour, 
LAST_DAY(date) as mon,
a.CongestionControl as cc,
AVG(ndt.a.MeanThroughputMbps) as avg_throughput, 
COUNT(ndt.a.MeanThroughputMbps) as cm,
COUNT(DISTINCT ndt.Client.IP) as cip,
APPROX_QUANTILES(ndt.a.MeanThroughputMbps, 100)[OFFSET(51)] as med_throughput , 
AVG(ndt.a.MinRTT) as avg_minRtt,
APPROX_QUANTILES(a.MinRTT, 100)[OFFSET(51)] as q_minRtt
FROM `measurement-lab.ndt.unified_downloads` as ndt 
where ndt.date >= date("2019-01-01") and
ndt.date < date("2021-09-01") and
ndt.server.Geo.CountryCode="DE" and
ndt.client.Geo.CountryCode="DE" and
ndt.server.geo.city = "Hamburg" and 
ndt.Client.Network.asnumber in (3320, 3209, 6805, 9145, 8881, 15943, 13045, 20676, 20880, 60294) 
group by hour, mon, cc order by mon, hour
' > throughput_rtt_busy_hours_ham.json

bq query --format=prettyjson --max_rows=1000000 --use_legacy_sql=false \
'
SELECT EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") as hour, 
LAST_DAY(date) as mon,
a.CongestionControl as cc,
AVG(ndt.a.MeanThroughputMbps) as avg_throughput, 
COUNT(ndt.a.MeanThroughputMbps) as cm,
COUNT(DISTINCT ndt.Client.IP) as cip,
APPROX_QUANTILES(ndt.a.MeanThroughputMbps, 100)[OFFSET(51)] as med_throughput , 
AVG(ndt.a.MinRTT) as avg_minRtt,
APPROX_QUANTILES(a.MinRTT, 100)[OFFSET(51)] as q_minRtt
FROM `measurement-lab.ndt.unified_downloads` as ndt 
where ndt.date >= date("2019-01-01") and
ndt.date < date("2021-09-01") and
ndt.server.Geo.CountryCode="DE" and
ndt.client.Geo.CountryCode="DE" and
ndt.server.geo.city = "Frankfurt" and 
ndt.Client.Network.asnumber in (3320, 3209, 6805, 198570, 8881, 8422, 42652, 20676, 60294, 41998) 
group by hour, mon, cc order by mon, hour
' > throughput_rtt_busy_hours_fra.json

bq query --format=prettyjson --max_rows=1000000 --use_legacy_sql=false \
'
SELECT EXTRACT(DAYOFYEAR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") as day,
COUNT(ndt.a.MeanThroughputMbps) as cm,
ndt.Client.IP as cip,
ndt.Client.Network.asnumber as asn
FROM `measurement-lab.ndt.unified_downloads` as ndt 
where ndt.date >= date("2021-01-01") and
ndt.date < date("2021-09-01") and
ndt.server.Geo.CountryCode="DE" and
ndt.client.Geo.CountryCode="DE" and
ndt.a.CongestionControl="bbr" and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") >= 20 and
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") < 22 and
((ndt.Client.Network.asnumber in (3320, 3209, 6805, 198570, 8881, 8422, 42652, 20676, 60294, 41998) and ndt.server.Geo.City="Frankfurt") or 
(ndt.Client.Network.asnumber in (3320, 3209, 6805, 9145, 8881, 15943, 13045, 20676, 20880, 60294) and ndt.server.Geo.City="Hamburg"))
group by day, cip, asn order by day
' > count_measurements_ips.json

bq query --format=prettyjson --max_rows=1000000 --use_legacy_sql=false \
'
SELECT EXTRACT(DAY from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") as day,
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") as hour,
COUNT(DISTINCT ndt.Client.IP) as cip,
COUNT(ndt.a.MeanThroughputMbps) as cm,
a.CongestionControl as cc,
ndt.server.Geo.City as city,
ndt.Client.Network.asnumber as asn
FROM `measurement-lab.ndt.unified_downloads` as ndt 
where ndt.date >= date("2021-08-01") and
ndt.date < date("2021-09-01") and
ndt.server.Geo.CountryCode="DE" and
ndt.client.Geo.CountryCode="DE" and
(ndt.Client.Network.asnumber in (3320, 3209, 6805, 198570, 8881, 8422, 42652, 20676, 60294, 41998) and ndt.server.Geo.City="Frankfurt")
group by day, asn, cc, hour, city order by day
' > count_ips_busy_hours_fra.json

bq query --format=prettyjson --max_rows=1000000 --use_legacy_sql=false \
'
SELECT EXTRACT(DAY from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") as day,
EXTRACT(HOUR from TIMESTAMP(a.TestTime) AT TIME ZONE "Europe/Berlin") as hour,
COUNT(DISTINCT ndt.Client.IP) as cip,
COUNT(ndt.a.MeanThroughputMbps) as cm,
a.CongestionControl as cc,
ndt.server.Geo.City as city,
ndt.Client.Network.asnumber as asn
FROM `measurement-lab.ndt.unified_downloads` as ndt 
where ndt.date >= date("2021-08-01") and
ndt.date < date("2021-09-01") and
ndt.server.Geo.CountryCode="DE" and
ndt.client.Geo.CountryCode="DE" and
(ndt.Client.Network.asnumber in (3320, 3209, 6805, 9145, 8881, 15943, 13045, 20676, 20880, 60294) and ndt.server.Geo.City="Hamburg")
group by day, asn, cc, hour, city order by day
' > count_ips_busy_hours_ham.json