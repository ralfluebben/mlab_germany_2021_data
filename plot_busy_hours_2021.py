import pandas as pd
import numpy as np
from datetime import datetime
from dateutil import relativedelta

from matplotlib import rcParams

rcParams['font.family'] = 'serif'
rcParams['font.sans-serif'] = ['Times']
rcParams['lines.markersize'] = 4
rcParams['font.size'] = 12

import matplotlib.pyplot as plt

df_throughput = pd.read_json('throughput_rtt_busy_hours_ham.json')

months_since_begin = []
begin_of_period = '2019-01-31'
date2 = datetime.strptime(begin_of_period, '%Y-%m-%d')

for i in range(df_throughput.shape[0]):
    date1 = datetime.strptime(df_throughput['mon'].iloc[i], '%Y-%m-%d')
    r = relativedelta.relativedelta(date1, date2)
    months_since_begin.append(r.months + (12*r.years))

df_throughput['months_since_begin'] = months_since_begin
fig, ax = plt.subplots()
m = [31, 31]
width = 0.9*(1.0/len(m))
my_legend = [ ["April 2021", 'Cubic'], ["Aug 2021", 'BBR'] ]
c=-len(m)/2.0*width+width/2
l_i=0
for i in m:
  cc = my_legend[l_i][1].lower()
  print(i,cc)
  df_month = df_throughput[((df_throughput['months_since_begin']==i) & (df_throughput['cc']==cc))]
  print(df_month)
  ax.bar(np.arange(0,24)+c, df_month["med_throughput"], width, label=my_legend[l_i][1])
  c=c+width
  l_i+=1

plt.xticks(np.arange(0,24))
plt.xlabel("hour of day")
plt.ylabel("throughput [Mbps]")
plt.xlim((-1,24))
plt.ylim((0,105))
#plt.legend()
plt.setp(ax.get_xticklabels(), rotation=90, ha="center", va='top', rotation_mode="default")
plt.grid(axis = 'y', linestyle = '--')
plt.tight_layout()
fname = "figures/busy_hours_ham_throughput_2021.pdf"
plt.savefig(fname)
plt.close()

fig, ax = plt.subplots()
c=-len(m)/2*width+width/2
l_i=0
for i in m:
  cc = my_legend[l_i][1].lower()
  df_month = df_throughput[((df_throughput['months_since_begin']==i) & (df_throughput['cc']==cc))]
  ax.bar(np.arange(0,24)+c, df_month["q_minRtt"], width, label=my_legend[l_i][1])
  c=c+width
  l_i+=1

plt.xticks(np.arange(0,24))
plt.xlim((-1,24))
plt.ylim((0,25))
plt.xlabel("hour of day")
plt.ylabel("RTT [ms]")
#plt.legend(loc='upper right')
plt.setp(ax.get_xticklabels(), rotation=90, ha="center", va='top', rotation_mode="default")
plt.grid(axis = 'y', linestyle = '--')
plt.tight_layout()
fname = "figures/busy_hours_ham_minrtt_2021.pdf"
plt.savefig(fname)
plt.close()

#####

df_throughput = pd.read_json('throughput_rtt_busy_hours_fra.json')

months_since_begin = []
begin_of_period = '2019-01-31'
date2 = datetime.strptime(begin_of_period, '%Y-%m-%d')

for i in range(df_throughput.shape[0]):
    date1 = datetime.strptime(df_throughput['mon'].iloc[i], '%Y-%m-%d')
    r = relativedelta.relativedelta(date1, date2)
    months_since_begin.append(r.months + (12*r.years))

df_throughput['months_since_begin'] = months_since_begin
fig, ax = plt.subplots()
m = [31, 31]
width = 0.9*(1.0/len(m))
my_legend = [ ["April 2021", 'Cubic'], ["Aug 2021", 'BBR'] ]
c=-len(m)/2.0*width+width/2
l_i=0
for i in m:
  cc = my_legend[l_i][1].lower()
  print(i,cc)
  df_month = df_throughput[((df_throughput['months_since_begin']==i) & (df_throughput['cc']==cc))]
  print(df_month)
  ax.bar(np.arange(0,24)+c, df_month["med_throughput"], width, label=my_legend[l_i][1])
  c=c+width
  l_i+=1

plt.xticks(np.arange(0,24))
plt.xlabel("hour of day")
plt.ylabel("throughput [Mbps]")
plt.xlim((-1,24))
plt.ylim((0,105))
plt.setp(ax.get_xticklabels(), rotation=90, ha="center", va='top', rotation_mode="default")
plt.legend()
plt.grid(axis = 'y', linestyle = '--')
plt.tight_layout()
fname = "figures/busy_hours_fra_throughput_2021.pdf"
plt.savefig(fname)
plt.close()

fig, ax = plt.subplots()
c=-len(m)/2*width+width/2
l_i=0
for i in m:
  cc = my_legend[l_i][1].lower()
  df_month = df_throughput[((df_throughput['months_since_begin']==i) & (df_throughput['cc']==cc))]
  ax.bar(np.arange(0,24)+c, df_month["q_minRtt"], width, label=my_legend[l_i][1])
  c=c+width
  l_i+=1

plt.xticks(np.arange(0,24))
plt.xlim((-1,24))
plt.ylim((0,25))
plt.xlabel("hour of day")
plt.ylabel("RTT [ms]")
plt.setp(ax.get_xticklabels(), rotation=90, ha="center", va='top', rotation_mode="default")
#plt.legend()
plt.grid(axis = 'y', linestyle = '--')
plt.tight_layout()
fname = "figures/busy_hours_fra_minrtt_2021.pdf"
plt.savefig(fname)
plt.close()