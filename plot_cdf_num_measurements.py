import numpy as np
import pandas as pd
from matplotlib import rcParams

rcParams['font.family'] = 'serif'
rcParams['font.sans-serif'] = ['Times']
rcParams['lines.markersize'] = 4
rcParams['font.size'] = 12
import matplotlib.pyplot as plt

start_date="2021-08-01"
end_date="2021-08-31"

daterange = pd.date_range(start_date, end_date)

one_non_cellular = None
more_non_cellular = None
one = None
more = None

for single_date in daterange:
    fn = "throughput_" + str(single_date.strftime("%Y-%m-%d")) + "_one_measurement_non_cellular.json"
    print(fn)
    df1 = pd.read_json(fn)
    if one_non_cellular is None:
        one_non_cellular = df1
    else:
        one_non_cellular=pd.concat([one_non_cellular, df1])

for single_date in daterange:
    fn = "throughput_" + str(single_date.strftime("%Y-%m-%d")) + "_more_measurement_non_cellular.json"
    print(fn)
    df1 = pd.read_json(fn)
    if more_non_cellular is None:
        more_non_cellular = df1
    else:
        more_non_cellular=pd.concat([more_non_cellular, df1])

for single_date in daterange:
    fn = "throughput_" + str(single_date.strftime("%Y-%m-%d")) + "_more_measurement.json"
    print(fn)
    df1 = pd.read_json(fn)
    if more is None:
        more = df1
    else:
        more=pd.concat([more, df1])

for single_date in daterange:
    fn = "throughput_" + str(single_date.strftime("%Y-%m-%d")) + "_one_measurement.json"
    print(fn)
    df1 = pd.read_json(fn)
    if one is None:
        one = df1
    else:
        one=pd.concat([one, df1])
    
n_bins = 1000

fig, ax = plt.subplots()
plt.grid(axis = 'y', linestyle = '--')
n, bins, patches = ax.hist(one["th"].to_numpy(), n_bins, density=True, histtype='step', cumulative=True, label='#mea=1 with cellular providers')
n, bins, patches = ax.hist(more["th"].to_numpy(), n_bins, density=True, histtype='step', cumulative=True, label='#mea>1 with cellular providers')
n, bins, patches = ax.hist(one_non_cellular["th"].to_numpy(), n_bins, density=True, histtype='step', cumulative=True, label='#mea=1 without cellular providers')
n, bins, patches = ax.hist(more_non_cellular["th"].to_numpy(), n_bins, density=True, histtype='step', cumulative=True, label='#mea>1 without cellular providers')

plt.legend(loc='lower right')
plt.xlim((0,100))
plt.xlabel("throughput [Mbps]")
plt.ylabel("P[throughput < x]")
plt.ylim((0, 1))
plt.tight_layout()
fname = "figures/one_more_measurements_cdf.pdf"
plt.savefig(fname)
