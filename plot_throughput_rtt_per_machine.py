import pandas as pd
import numpy as np
from datetime import datetime, date
from dateutil import relativedelta

from matplotlib import rcParams

rcParams['font.family'] = 'serif'
rcParams['font.sans-serif'] = ['Times']
rcParams['lines.markersize'] = 4
rcParams['font.size'] = 12

import matplotlib.pyplot as plt

def get_forward_month_list(start_date):
    start_month = date.fromisoformat(start_date)
    return [(start_month - relativedelta.relativedelta(months=i)).strftime('%b %y') for i in range(0,32)]


df_throughput = pd.read_json('throughput_per_month_asns_busy_hours.json')

months_since_begin = []
begin_of_period = '2019-01-31'
date2 = datetime.strptime(begin_of_period, '%Y-%m-%d')

for i in range(df_throughput.shape[0]):
    date1 = datetime.strptime(df_throughput['mon'].iloc[i], '%Y-%m-%d')
    r = relativedelta.relativedelta(date1, date2)
    months_since_begin.append(r.months + (12*r.years))

df_throughput['months_since_begin'] = months_since_begin

locations = [   #["mlab0", "fra01", 19, 7],
                ["mlab1", "fra01", 19, 7], 
                ["mlab2", "fra01", 19, 7],
                ["mlab3", "fra01", 19, 7], 
                #["mlab0", "fra02", 19, 7],
                ["mlab1", "fra02", 19, 7], 
                ["mlab2", "fra02", 19, 7],
                ["mlab3", "fra02", 19, 7],
                #["mlab0", "fra03", 19, 7],
                ["mlab1", "fra03", 19, 7], 
                ["mlab2", "fra03", 19, 7],
                ["mlab3", "fra03", 19, 7],
                #["mlab0", "fra04", 19, 7],
                ["mlab1", "fra04", 19, 7], 
                ["mlab2", "fra04", 19, 7],
                ["mlab3", "fra04", 19, 7],
                #["mlab0", "fra04", 19, 7],
                ["mlab1", "fra05", 19, 7], 
                ["mlab2", "fra05", 19, 7],
                ["mlab3", "fra05", 19, 7],
                #["mlab0", "fra04", 19, 7],
                ["mlab1", "fra06", 19, 7], 
                ["mlab2", "fra06", 19, 7],
                ["mlab3", "fra06", 19, 7], 
                #["mlab0", "ham02", 19, 10],
                ["mlab1", "ham02", 19, 10], 
                ["mlab2", "ham02", 19, 10],
                ["mlab3", "ham02", 19, 10]]

month_labels = get_forward_month_list("2021-08-01")
month_labels.reverse()

width = 0.22

fig, ax = plt.subplots()

months = [12, 14, 27, 31]
cc_month =['cubic', 'cubic', 'bbr', 'bbr']
xlabels = []
bar_color=['cyan', 'orange', 'green', 'red']
bar_color=['C0', 'C1', 'C2', 'C3']
my_legend = [ [ "Jan 2020", 'cubic' ], ["Mar 2020", 'cubic'], ["Apr 2021", 'bbr'], ["Aug 2021", 'bbr'] ]
x=0
m=15
machine="mlab1"
site_prev="fra01"
skip=False
for l in locations:
   
    machine = l[0]
    site = l[1]
    c=-len(months)/2*width+width/2
    i=0

    if site_prev != site:
        site_prev=site
        if skip==True:
            skip=False
        else:
            skip=True
                 #ax.plot([x-0.5, x-0.5], [0, 35], 'k--')
            ax.axvspan(x-0.5, x+2.5, facecolor='grey', alpha=0.3, zorder=-100) 

    for m in months:
        cc=cc_month[i]
        df_bbr = df_throughput[((df_throughput['months_since_begin'] == m)  & (df_throughput['cc'] == cc) & (df_throughput['ma'] == machine) & (df_throughput['site'] == site))]

        print(m,l,df_bbr)

        data_tag = 'med_throughput'
        y1 = df_bbr[data_tag].to_numpy()
    
        data_tag = 'q25_throughput'
        y1_q25 = df_bbr[data_tag].to_numpy()

        data_tag = 'q75_throughput'
        y1_q75 = df_bbr[data_tag].to_numpy()

        if(len(y1)>0):
            la=None
            if x==1:
                la = my_legend[i][0]
            ax.bar(x+c, y1[0], width, color=[bar_color[i]], label=la)
        c+=width
        i+=1
    if "ham" in site:
        s=site.replace("ham0",'h')
    if "fra" in site:
        s=site.replace("fra0",'f')
    m = machine.replace("mlab","")         
    la = s + "_" + m
    xlabels.append(la)
    x+=1

#plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15), 
#          fancybox=True, shadow=True, ncol=4)
plt.xlabel("site")
plt.ylabel("throughput [MBit/s]")
xticks = np.arange(0,x)
plt.xlim((-1,21))
plt.ylim((0,35))
plt.xticks(xticks,xlabels)
plt.setp(ax.get_xticklabels(), rotation=90, ha="right",va="center", rotation_mode="anchor")
plt.grid(axis = 'y', linestyle = '--')
plt.tight_layout()
fname = "figures/throughput_all_machines_per_month.pdf"
plt.savefig(fname)
plt.close()

fig, ax = plt.subplots()
xlabels = []
x=0
m=15
machine="mlab1"
site_prev="fra01"
skip=False
for l in locations:
   
    machine = l[0]
    site = l[1]
    c=-len(months)/2*width+width/2
    i=0

    if site_prev != site:
        site_prev=site
        if skip==True:
            skip=False
        else:
            skip=True
                    #ax.plot([x-0.5, x-0.5], [0, 35], 'k--')
            ax.axvspan(x-0.5, x+2.5, facecolor='grey', alpha=0.3, zorder=-100) 

    for m in months:
        cc=cc_month[i]
        df_bbr = df_throughput[((df_throughput['months_since_begin'] == m)  & (df_throughput['cc'] == cc) & (df_throughput['ma'] == machine) & (df_throughput['site'] == site))]

        print(m,l,df_bbr)

        data_tag = 'med_minRtt'
        y1 = df_bbr[data_tag].to_numpy()
    
        data_tag = 'q25_minRtt'
        y1_q25 = df_bbr[data_tag].to_numpy()

        data_tag = 'q75_minRtt'
        y1_q75 = df_bbr[data_tag].to_numpy()

        if(len(y1)>0):
            la=None
            if x==1:
                la = my_legend[i][0]
            ax.bar(x+c, y1[0], width, color=[bar_color[i]], label=la)
        c+=width
        i+=1
    
    if "ham" in site:
        s=site.replace("ham0",'h')
    if "fra" in site:
        s=site.replace("fra0",'f')
    m = machine.replace("mlab","")         
    la = s + "_" + m
    xlabels.append(la)
    x+=1

#plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15), 
#          fancybox=True, shadow=True, ncol=4)
plt.xlabel("site")
plt.ylabel("RTT [ms]")
xticks = np.arange(0,x)
plt.xlim((-1,21))
plt.ylim((0,35))
plt.xticks(xticks,xlabels)
plt.setp(ax.get_xticklabels(), rotation=90, ha="right",va="center", rotation_mode="anchor")
plt.grid(axis = 'y', linestyle = '--')
plt.tight_layout()
fname = "figures/rtt_all_machines_per_month.pdf"
plt.savefig(fname)
plt.close()

fig, ax = plt.subplots()
xlabels = []
x=0
m=15
machine="mlab1"
site_prev="fra01"
skip=False
for l in locations:
   
    machine = l[0]
    site = l[1]
    c=-len(months)/2*width+width/2
    i=0

    if site_prev != site:
        site_prev=site
        if skip==True:
            skip=False
        else:
            skip=True
            ax.axvspan(x-0.5, x+2.5, facecolor='grey', alpha=0.3, zorder=-100) 

    for m in months:
        cc=cc_month[i]
        df_bbr = df_throughput[((df_throughput['months_since_begin'] == m)  & (df_throughput['cc'] == cc) & (df_throughput['ma'] == machine) & (df_throughput['site'] == site))]

        print(m,l,df_bbr)

        data_tag = 'cm'
        y1 = df_bbr[data_tag].to_numpy()
    
        if(len(y1)>0):
            la=None
            if x==1:
                la = my_legend[i][0]
            ax.bar(x+c, y1[0]/1000, width, color=[bar_color[i]], label=la)
        c+=width
        i+=1
    
    if "ham" in site:
        s=site.replace("ham0",'h')
    if "fra" in site:
        s=site.replace("fra0",'f')
    m = machine.replace("mlab","")         
    la = s + "_" + m
    xlabels.append(la)
    x+=1

plt.legend(ncol=2)
plt.xlabel("site")
plt.ylabel("#measurements [in thousands]")
xticks = np.arange(0,x)
plt.xlim((-1,21))
plt.xticks(xticks,xlabels)
plt.setp(ax.get_xticklabels(), rotation=90, ha="right",va="center", rotation_mode="anchor")
plt.grid(axis = 'y', linestyle = '--')
plt.tight_layout()
fname = "figures/count_all_machines_per_month.pdf"
plt.savefig(fname)
plt.close()