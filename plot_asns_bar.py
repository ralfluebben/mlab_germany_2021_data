import pandas as pd
import numpy as np
from datetime import datetime
from dateutil import relativedelta
import json
from matplotlib import rcParams
rcParams['font.family'] = 'serif'
rcParams['font.sans-serif'] = ['Times']
rcParams['lines.markersize'] = 4
rcParams['font.size'] = 12

import matplotlib.pyplot as plt

cities = [ "hamburg", "frankfurt"]
datanames = [ "cdf_throughput", "cdf_minRtt"]
for dn in datanames:
    if dn == "cdf_throughput":
        yl = "throughput [Mbps]"
        yli = (0,300)
    elif dn == "cdf_minRtt":
        yl = "RTT [ms]"
        yli = (0,100)
    
    for city in cities:
        fn='throughput_asns_'+ city +'.json'
        df_throughput = pd.read_json(fn)

        months_since_begin = []
        begin_of_period = '2019-01-31'
        date2 = datetime.strptime(begin_of_period, '%Y-%m-%d')

        for i in range(df_throughput.shape[0]):
            date1 = datetime.strptime(df_throughput['mon'].iloc[i], '%Y-%m-%d')
            r = relativedelta.relativedelta(date1, date2)
            months_since_begin.append(r.months + (12*r.years))

        df_throughput['months_since_begin'] = months_since_begin

        fig, ax = plt.subplots()

        months = [12, 31]
        # bring ASNs in a similar order at least for that occur in Frankfirt and Hamburg
        if city == "hamburg":
            m=[3209, 3320, 6805, 8881, 20676, 60294, 9145, 13045, 15943, 20880]
        elif city == "frankfurt":
            m=[3209, 3320, 6805, 8881, 20676, 60294, 8422, 41998, 42652, 198570]

        width=0.45
        offset=-width/2

        for month in months:
            c=0
            for i in m:
                df_asn = df_throughput[((df_throughput['months_since_begin']==month) & (df_throughput['asn']==i))]
                if(len(df_asn[dn])):
                    t=np.array(df_asn[dn].to_numpy()[0]).astype('float')[1:]
                    try:
                        yerr = list([t[24], t[74]])
                        yerr = np.matrix([[t[24]], [t[74]]])
                        print(yerr, t[49])
                        if month==31:
                            hatch='\\\\'
                            alpha=0.8
                        else:
                            hatch=None
                            alpha=1
                        ax.bar(c+offset, t[49], width , yerr=yerr, hatch=hatch, alpha=alpha)
                        
                    except Exception as e:
                        print(e)
                    c+=1
            offset+=width

        plt.xticks(range(len(m)),m)
        plt.xlabel("ASN")
        plt.ylabel(yl)
        plt.setp(ax.get_xticklabels(), rotation=40, ha="center", va='top', rotation_mode="default")
        plt.ylim(yli)
        plt.grid(axis = 'y', linestyle = '--')
        plt.tight_layout()
        fname = "figures/provider_" + city + '_' + dn + '.pdf'
        plt.savefig(fname)
        plt.close()